const { Router } = require('express')
const jwt = require('jsonwebtoken')

class RouterCustom {
    constructor(){
        this.router = Router() // instanciar const router = Router()
        this.init()
    }

    getRouter(){
        return this.router // module.exports 
    }

    init(){} // en clases heradadas las vamos a definir

    generateCustomResponses = ( req, res, next ) => {
        res.sendSuccess = payload => res.send({status: 'success', payload})
        res.sendServerError = error => res.status(500).send({satust: 'error', error})
        res.sendUserError = error => res.status(400).send({status: 'error', error })
        next()
    }  

    // método para ejecutar nuestros callback [middl...., (req, res) => {}]    
    applyCallbacks(callbacksArray){ // (req, res, next)
        return callbacksArray.map(callback => async (...params) => {
            try{
                await callback.apply(this, params)
            } catch(error) {
                console.log(error)
                params[1].status(500).send(error) // response
            }
    
        })
    }
    // policies [ 'PUBLIC', 'USER', 'USER_PREMIUM', 'ADMIN'] // se puede cambiar cookies 
    handlePolicies = policies => (req, res, next ) => {
        // req.cookies.token
        if(policies[0] === 'PUBLIC') return next()
        const authHeaders = req.headers.authorization // 'Bearer afsdlkfhashfdhjasfdkhsafdkha'
        if(!authHeaders) return res.status(401).send({stauts: 'error', error: 'Unauthorized'})
        const token = authHeaders.split(' ')[1] // <- ['Breare', 'akljsfdjasfhkhsjfda'],
        let user = jwt.verify(token, 'CoderSecretoJesonWebToken')
        if(!policies.includes(user.role.toUpperCase())) return res.status(403).send({status: 'error', error: 'Not permissions'})
        req.user = user
        next()
    } 

    get(path, policies, ...callbacksArray){
        this.router.get(path, this.handlePolicies(policies), this.generateCustomResponses, this.applyCallbacks(callbacksArray))
    }
    post(path, policies, ...callbacksArray){
        this.router.post(path, this.handlePolicies(policies), this.generateCustomResponses, this.applyCallbacks(callbacksArray))
    }
    put(path, policies, ...callbacksArray){
        this.router.put(path, this.handlePolicies(policies), this.generateCustomResponses, this.applyCallbacks(callbacksArray))
    }
    delete(path, policies, ...callbacksArray){
        this.router.delete(path, this.handlePolicies(policies), this.generateCustomResponses, this.applyCallbacks(callbacksArray))
    }
    

}

module.exports = RouterCustom
