const { Router } = require('express')
const UserController = require('../../controllers/users.controller')

const router = Router()
const {
    getUsers,
    createUser,
    updateUser,
    deleteUser
} = new UserController()

router.get('/', getUsers)
router.post('/', createUser)
router.put('/:uid',  updateUser)
router.delete('/:uid', deleteUser)

module.exports = router


