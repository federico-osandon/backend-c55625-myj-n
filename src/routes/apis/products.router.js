const { Router } = require('express')
const ProductController = require('../../controllers/products.controller')

const router = Router()
const productController = new ProductController

router.get('/',    productController.getProducts)

router.get('/',    productController.getProduct)
router.post('/',   productController.createProduct)
router.put('/',    productController.updateProduct)
router.delete('/', productController.deleteProduct)

module.exports = router