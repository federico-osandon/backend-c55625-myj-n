const ProductDaoMongo = require("../daos/Mongo/productManagerMongo")

class ProductController {
    constructor(){
        this.productsService = new ProductDaoMongo()        
    }

    getProducts = async (req, res) => {
        try {
            const products = await this.productsService.getProducts()
            // console.log(products)
            res.send({
                status: 'success',
                payload: products
            })
            
        } catch (error) {
            console.log(error)
        }
    }

    getProduct = async (req,res )=> {
       res.send('getPRoduct')
    }

    createProduct = async (req, res) => {
        try {
            const newProduct = req.body
            if (!newProduct.title || !newProduct.stock ) {
                return res.status(400).send({status: 'error', error: 'Faltan mandar campos obligatorios'})
            }
            const result = await this.productsService.createProduct(newProduct)
            
            res.send({status: 'success', payload: result})
        } catch (error) {
            console.log(error)
        }
    }

    updateProduct= async (req, res) => {
        res.send('update Product')
    }

    deleteProduct = async (req, res) => {
        res.send('delete productos')
    }

}

module.exports = ProductController